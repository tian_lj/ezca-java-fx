package org.ezca.javafxclient;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.net.URL;

/**
 * @Author tianlj
 * @Date 2023 02 10 13 49
 **/
public class AppStartup extends Application {


    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader();
//        StackPane stackPane = fxmlLoader.load(new FileInputStream(this.getClass().getResource("/fxml/login/login.fxml").getPath()));
//        URL uRl = fxmlLoader.getClassLoader().getResource("fxml/login/login.fxml");
        URL uRl = fxmlLoader.getClassLoader().getResource("fxml/main/main.fxml");


        fxmlLoader.setLocation(uRl);
        StackPane stackPane = fxmlLoader.load();


        Scene scene = new Scene(stackPane);
        stage.setScene(scene);
//        stage.setMaxHeight(450);
//        stage.setMaxWidth(650);
        //可以改变窗口大小
        stage.setResizable(false);
        //设置窗口图标
        stage.getIcons().add(new Image(this.getClass().getResource("/img/favicon.ico").toString()));
        stage.show();
    }

    public static void main(String[] args) {
        System.out.println("main====" + Thread.currentThread().getName());
        launch(args);
    }
}
