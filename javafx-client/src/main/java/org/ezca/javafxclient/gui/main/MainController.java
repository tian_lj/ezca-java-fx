package org.ezca.javafxclient.gui.main;

import com.jfoenix.assets.JFoenixResources;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import lombok.SneakyThrows;
import org.ezca.javafxclient.AppStartup;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;

/**
 * @Author tianlj
 * @Date 2023 02 10 16 50
 **/
public class MainController {

    @FXML
    private TreeView menuvVew;

    @FXML
    private ImageView logo;

    @FXML
    public void initialize() {

        //初始化菜单

        TreeItem rootItem = new TreeItem("Tutorials");

        TreeItem webItem = new TreeItem("Web Tutorials");
        webItem.getChildren().add(new TreeItem("HTML  Tutorial"));
        webItem.getChildren().add(new TreeItem("HTML5 Tutorial"));
        webItem.getChildren().add(new TreeItem("CSS Tutorial"));
        webItem.getChildren().add(new TreeItem("SVG Tutorial"));
        rootItem.getChildren().add(webItem);

        TreeItem javaItem = new TreeItem("Java Tutorials");
        javaItem.getChildren().add(new TreeItem("Java Language"));
        javaItem.getChildren().add(new TreeItem("Java Collections"));
        javaItem.getChildren().add(new TreeItem("Java Concurrency"));
        rootItem.getChildren().add(javaItem);

        menuvVew.setRoot(rootItem);

        menuvVew.setShowRoot(false);

//        Image image = new Image(this.getClass().getResource("/images/logo.png").toString());
//
//        logo.setImage(image);

        System.out.println(
                "init"
        );
    }


    @SneakyThrows
    @PostConstruct
    public void init() {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader();
        StackPane stackPane = fxmlLoader.load(new FileInputStream(this.getClass().getResource("fxml/login/login.fxml").getPath()));
        Scene scene = new Scene(stackPane);
        double width = 650;
        double height = 470;
        final ObservableList<String> stylesheets = scene.getStylesheets();
        stylesheets.addAll(JFoenixResources.load("css/jfoenix-fonts.css").toExternalForm(),
//                JFoenixResources.load("css/jfoenix-design.css").toExternalForm(),
                AppStartup.class.getResource("/css/jfoenix-main-demo.css").toExternalForm()
//                AppStartup.class.getResource("/css/login.css").toExternalForm()
        );
        stage.setScene(scene);
        //设置窗口图标
        stage.getIcons().add(new Image(this.getClass().getResource("/img/favicon.ico").toString()));
        stage.show();
    }
}
