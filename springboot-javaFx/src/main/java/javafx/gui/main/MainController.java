package javafx.gui.main;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.extra.spring.SpringUtil;
import de.felixroske.jfxsupport.FXMLController;
import javafx.AppStartup;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.gui.admin.AdminView;
import javafx.gui.setting.SettingView;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTreeCell;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import javafx.util.StringConverter;


/**
 * @Author tianlj
 * @Date 2023 02 10 16 50
 **/
@FXMLController
public class MainController {

    @FXML
    private TreeView menuvVew;


    @FXML
    Pane myDynamicPane;


    @FXML
    public void initialize() {

        //初始化菜单

        TreeItem<MenuBean> rootItem = new TreeItem<MenuBean>(new MenuBean("root", null));

        TreeItem<MenuBean> userItem = new TreeItem<MenuBean>(new MenuBean("系统用户", null));
        userItem.getChildren().add(new TreeItem<MenuBean>(new MenuBean("管理员配置", AdminView.class)));
//        webItem.getChildren().add(new TreeItem("HTML5 Tutorial"));
//        webItem.getChildren().add(new TreeItem("CSS Tutorial"));
//        webItem.getChildren().add(new TreeItem("SVG Tutorial"));
        rootItem.getChildren().add(userItem);

        TreeItem<MenuBean> settingItem = new TreeItem<MenuBean>(new MenuBean("系统设置", null));
        settingItem.getChildren().add(new TreeItem<MenuBean>(new MenuBean("设置", SettingView.class)));
        rootItem.getChildren().add(settingItem);

        menuvVew.setRoot(rootItem);

        menuvVew.setShowRoot(false);
        menuvVew.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);


        menuvVew.setCellFactory(TextFieldTreeCell.forTreeView(new StringConverter<MenuBean>() {
            @Override
            public String toString(MenuBean object) {
                return object.getTitle();
            }

            @Override
            public MenuBean fromString(String string) {
                return null;
            }
        }));

        menuvVew.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<MenuBean>>() {
            @Override
            public void changed(ObservableValue<? extends TreeItem<MenuBean>> observable, TreeItem<MenuBean> oldValue, TreeItem<MenuBean> newValue) {
                MenuBean menuBean = newValue.getValue();
                if (ObjectUtil.isNotNull(menuBean.getAbstractFxmlView())) {
                    Parent parent = SpringUtil.getBean(newValue.getValue().getAbstractFxmlView()).getView();
                    myDynamicPane.getChildren().clear();
                    myDynamicPane.getChildren().add(parent);
                }


            }

        });


//        Image image = new Image(this.getClass().getResource("/images/logo.png").toString());
//
//        logo.setImage(image);


//        menuvVew.setOnMouseClicked(new EventHandler<MouseEvent>() {
//            @Override
//            public void handle(MouseEvent event) {
//                System.out.println(event);
//            }
//        });

        System.out.println(
                "init"
        );
    }


}
