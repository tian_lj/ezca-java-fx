package javafx.gui.main;

import de.felixroske.jfxsupport.AbstractFxmlView;

/**
 * @ClassName MenuBean
 * @Description TODO
 * @Author tianlj
 * @Date 2023/2/13 21:08
 * @Version 1.0
 */
public class MenuBean {

    private String title;
    private Class<? extends AbstractFxmlView>  abstractFxmlView;


    public MenuBean(String title, Class<? extends AbstractFxmlView>  abstractFxmlView) {
        this.abstractFxmlView = abstractFxmlView;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public Class<? extends AbstractFxmlView> getAbstractFxmlView() {
        return abstractFxmlView;
    }

    public void setAbstractFxmlView(Class<? extends AbstractFxmlView> abstractFxmlView) {
        this.abstractFxmlView = abstractFxmlView;
    }
}
