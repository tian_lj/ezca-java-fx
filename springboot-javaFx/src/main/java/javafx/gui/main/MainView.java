package javafx.gui.main;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

/**
 * @Author tianlj
 * @Date 2023 02 13 14 21
 **/
@FXMLView("/fxml/main/main2.fxml")
public class MainView extends AbstractFxmlView {

}
