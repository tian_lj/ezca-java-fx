package javafx.gui.table;

import java.util.ArrayList;

/**
 * @Author tianlj
 * @Date 2023 02 20 17 40
 **/
public class TablePageList {

    private Integer total;

    private ArrayList<String> rows;


    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public ArrayList<String> getRows() {
        return rows;
    }

    public void setRows(ArrayList<String> rows) {
        this.rows = rows;
    }
}
