package javafx.gui.admin;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @Author tianlj
 * @Date 2023 02 20 17 47
 **/
public class Admin {
    private StringProperty userId = null;
    private StringProperty loginName = null;
    private StringProperty userName = null;
    private StringProperty userStatus = null;

    public String getUserId() {
        return userId.get();
    }

    public StringProperty userIdProperty() {
        if (userId == null) userId = new SimpleStringProperty(this, "userId");
        return userId;
    }

    public void setUserId(String userId) {
        userIdProperty().set(userId);
    }

    public String getLoginName() {
        return loginName.get();
    }

    public StringProperty loginNameProperty() {
        if (loginName == null) loginName = new SimpleStringProperty(this, "loginName");
        return loginName;
    }

    public void setLoginName(String loginName) {
        loginNameProperty().set(loginName);
    }

    public String getUserName() {
        return userName.get();
    }

    public StringProperty userNameProperty() {
        if (userName == null) userName = new SimpleStringProperty(this, "userName");
        return userName;
    }

    public void setUserName(String userName) {
        userNameProperty().set(userName);
    }

    public String getUserStatus() {
        return userStatus.get();
    }

    public StringProperty userStatusProperty() {
        if (userStatus == null) userStatus = new SimpleStringProperty(this, "userStatus");
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        userStatusProperty().set(userStatus);
    }

    public Admin(String userId, String loginName, String userName, String userStatus) {
        setUserId(userId);
        setLoginName(loginName);
        setUserName(userName);
        setUserStatus(userStatus);
    }
}
