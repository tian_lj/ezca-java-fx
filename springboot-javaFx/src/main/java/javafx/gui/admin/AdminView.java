package javafx.gui.admin;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

/**
 * @ClassName AdminView
 * @Description TODO
 * @Author tianlj
 * @Date 2023/2/13 21:46
 * @Version 1.0
 */
@FXMLView("/fxml/admin/admin.fxml")
public class AdminView extends AbstractFxmlView {


}
