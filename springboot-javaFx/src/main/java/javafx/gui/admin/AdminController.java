package javafx.gui.admin;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.ejlchina.okhttps.HTTP;
import com.ejlchina.okhttps.WebSocket;
import de.felixroske.jfxsupport.FXMLController;
import javafx.fxml.FXML;
import javafx.gui.table.TablePageList;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @Author tianlj
 * @Date 2023 02 14 09 25
 **/
@FXMLController
public class AdminController {

    @FXML
    private TableView tabableView;

    @FXML
    private TextField userNameText;

    @FXML
    private Label pageIdnex;

    @FXML
    private Label totalLabel;

    @FXML
    private Button previousBtn;

    @FXML
    private Button nextBtn;

    @FXML
    private ChoiceBox pageSizeChoiceBox;


    private int pageSize;

    private int total;

    private int maxPageIndex;

    private List<Admin> members;


    @FXML
    public void initialize() {
        //初始化 每页条数
        pageSizeChoiceBox.getItems().addAll(10, 20, 30);
        pageSizeChoiceBox.setValue(10);
        pageSize = 10;

        //列名
        TableColumn<Admin, String> userId =
                new TableColumn<>("ID");
        //每行的键
        userId.setCellValueFactory(
                new PropertyValueFactory<>("userId"));

        TableColumn<Admin, String> loginName =
                new TableColumn<>("登录账号");
        //每行的键
        loginName.setCellValueFactory(
                new PropertyValueFactory<>("loginName"));

        TableColumn<Admin, String> userName =
                new TableColumn<>("登录名称");
        //每行的键
        userName.setCellValueFactory(
                new PropertyValueFactory<>("userName"));


        TableColumn<Admin, String> userStatus =
                new TableColumn<>("状态");
        //每行的键
        userStatus.setCellValueFactory(
                new PropertyValueFactory<>("userStatus"));


        tabableView.getColumns().addAll(userId, userName, userStatus);

        tabableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        try {
            query(1);
            initPagination();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 初始化分页组件
     */
    private void initPagination() {
        String currentPageIndex = "1";
        pageIdnex.setText(currentPageIndex);
        previousBtn.setDisable(true);
        if (maxPageIndex <= 1) {
            nextBtn.setDisable(true);
        }
    }

    /**
     * 查询按钮事件
     *
     * @throws IOException
     */
    @FXML
    private void queryBtn() throws IOException {
        Map<String, Object> query = new HashMap<>();
        query.put("searchKey", userNameText.getText());
        query.put("pageNum", 1);
        query.put("pageSize", pageSize);
        getTableData(query);
        totalLabel.setText(Convert.toStr(total));
    }

    public void query(int pageNum) throws IOException {
        Map<String, Object> query = new HashMap<>();
        query.put("searchKey", userNameText.getText());
        query.put("pageNum", pageNum);
        query.put("pageSize", pageSize);
        getTableData(query);
        totalLabel.setText(Convert.toStr(total));
    }

    /**
     * 上一页
     */
    @FXML
    private void previous() throws IOException {
        String currentPageIndex = pageIdnex.getText();
        int previous = Convert.toInt(currentPageIndex) - 1;
        if (previous <= 1) {
            previousBtn.setDisable(true);
            return;
        }
        pageIdnex.setText(Convert.toStr(previous));
        query(previous);
    }

    /**
     * 下一页
     */
    @FXML
    private void next() throws IOException {
        String currentPageIndex = pageIdnex.getText();
        int next = Convert.toInt(currentPageIndex) + 1;
        if (next >= maxPageIndex) {
            nextBtn.setDisable(true);
            return;
        }
        pageIdnex.setText(Convert.toStr(next));
        query(next);
    }

    @FXML
    private void reset() {
        userNameText.setText("");
    }

    public void getTableData(Map<String, Object> query) {
        System.out.println("请求参数{}" + query);
        HTTP http = HTTP.builder()
                .baseUrl("ws://127.0.0.1:8185")
                .build();
        WebSocket ws1 = http.webSocket("/sysUser")
                .setOnMessage((WebSocket ws, WebSocket.Message msg) -> {
                    TablePageList tablePageList = JSONUtil.toBean(msg.toString(), TablePageList.class);
                    total = tablePageList.getTotal();
                    maxPageIndex = total / pageSize + 1;
                    ArrayList<String> rows = tablePageList.getRows();
                    members = new ArrayList<>();
                    if (ObjectUtil.isNotEmpty(rows)) {
                        for (String row : rows) {
                            JSONObject jsonObject = JSONUtil.parseObj(row);
                            Admin admin = new Admin(jsonObject.get("userId").toString(), jsonObject.get("loginName").toString(), jsonObject.get("userName").toString(), jsonObject.get("userStatus").toString());
                            members.add(admin);
                        }
                    }
                    tabableView.getItems().clear();
                    tabableView.getItems().addAll(members);
                    System.out.println(msg);

                })
                .listen();
        ws1.send(JSONUtil.parseObj(query).toString());
        ws1.cancel();
    }
}
