package javafx.gui.setting;

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

/**
 * @ClassName SettingView
 * @Description TODO
 * @Author tianlj
 * @Date 2023/2/13 21:56
 * @Version 1.0
 */
@FXMLView("/fxml/setting/setting.fxml")
public class SettingView extends AbstractFxmlView {
}
