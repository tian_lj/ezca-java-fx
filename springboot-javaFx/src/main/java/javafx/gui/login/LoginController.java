package javafx.gui.login;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXTextField;
import de.felixroske.jfxsupport.FXMLController;
import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author tianlj
 * @Date 2023 02 10 14 44
 **/
@FXMLController
public class LoginController {

    @FXML
    private StackPane rootPane;
    @FXML
    private Pane imagePane;

    @FXML
    private StackPane centerPane;


    @FXML
    private Label errorLabel;
    @FXML
    private Label userIcon;
    @FXML
    private Label pwdIcon;
    @FXML
    private JFXProgressBar lodingBar;
    @FXML
    private JFXTextField userNameTextField;
    @FXML
    private JFXPasswordField passWordTextField;

    private DoubleProperty imageWidth = new SimpleDoubleProperty();
    private DoubleProperty imageHeiht = new SimpleDoubleProperty();

    @FXML
    private JFXButton loginBut;

    public LoginController() {

    }

    @FXML
    public void initialize() {
        initAction();
        System.out.println(
                "init"
        );
    }

    private void initAction() {
        errorLabel.visibleProperty().bind(errorLabel.textProperty().isNotEmpty());
        errorLabel.managedProperty().bind(errorLabel.visibleProperty());

        lodingBar.visibleProperty().bind(centerPane.disableProperty());
        lodingBar.managedProperty().bind(lodingBar.visibleProperty());

        userNameTextField.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                userNameTextField.validate();
            }
        });
        passWordTextField.focusedProperty().addListener((o, oldVal, newVal) -> {
            if (!newVal) {
                passWordTextField.validate();
            }
        });

        loginBut.disableProperty().bind(Bindings.or(
                userNameTextField.textProperty().isEqualTo(""),
                passWordTextField.textProperty().isEqualTo("")));


        rootPane.setOnKeyPressed(event -> {
            System.out.println(loginBut.isDisable());
            if (event.getCode() == KeyCode.ENTER) {
                if (loginBut.isDisable() == false) {
                    try {
                        login();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        });
    }

    @FXML
    private void login() throws IOException {

        System.out.println("登录");
        String userName = userNameTextField.getText();
        String pwd = passWordTextField.getText();

        centerPane.setDisable(true);
        loginBut.setText("正在登录...");

        Map<String, Object> rel = new HashMap<>();
        if (userName.equals("admin")) {
            rel.put("code", 0);
            rel.put("msg", "成功");
            errorLabel.setText("");
            System.out.println("登录成功");

            FXMLLoader fxmlLoader = new FXMLLoader();
            URL uRl = fxmlLoader.getClassLoader().getResource("fxml/main/main.fxml");
            fxmlLoader.setLocation(uRl);
            StackPane stackPane = fxmlLoader.load();

            Stage stage = new Stage();
            //设置窗口图标
            stage.getIcons().add(new Image(this.getClass().getResource("/img/favicon.ico").toString()));
            Scene scene = new Scene(stackPane);
            stage.setScene(scene);
            stage.show();

            try {
                Window window = loginBut.getScene().getWindow();

                if (window instanceof Stage) {

                    ((Stage) window).close();

                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            rel.put("code", -1);
            rel.put("msg", "失败");
            lodingBar.requestFocus();
            errorLabel.setText("登录失败");
        }
        centerPane.setDisable(false);
        loginBut.setText("登录");
    }


}
