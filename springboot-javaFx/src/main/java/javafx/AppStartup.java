package javafx;

/**
 * @Author tianlj
 * @Date 2023 02 10 11 56
 **/

import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import javafx.gui.main.MainView;
import javafx.stage.Stage;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppStartup extends AbstractJavaFxApplicationSupport {


    @Override
    public void start(Stage stage) throws Exception {
        super.start(stage);
    }

    public static void main(String[] args) {
        //设置不要启动动画
        MysplashScreen splashScreen = new MysplashScreen();
        splashScreen.setVisible(false);
        launch(AppStartup.class, MainView.class, splashScreen, args);
    }
}
