package javafx;

import de.felixroske.jfxsupport.SplashScreen;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * @Author tianlj
 * @Date 2023 02 13 14 49
 **/
public class MysplashScreen extends SplashScreen {

    private static String DEFAULT_IMAGE = "/splash/javafx.png";

    private boolean visible;
    private String image = "";

    public MysplashScreen() {
    }

    public Parent getParent() {
        ImageView imageView = new ImageView(this.getClass().getResource(this.getImagePath()).toExternalForm());
        ProgressBar splashProgressBar = new ProgressBar();
        splashProgressBar.setPrefWidth(imageView.getImage().getWidth());
        VBox vbox = new VBox();
        vbox.getChildren().addAll(new Node[]{imageView, splashProgressBar});
        return vbox;
    }

    public boolean visible() {
        return false;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getImagePath() {
        if ("".equals(this.image)) {
            return DEFAULT_IMAGE;
        } else {
            return this.image;
        }

    }
}
