package org.ezca.demo1;

import javafx.application.Application;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.util.function.DoubleToIntFunction;


/**
 * @Author tianlj
 * @Date 2022 12 06 21 21
 * Group 相关知识
 **/
public class GroupMain extends Application {

    int i = 20;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Button b1 = new Button("b1");
        Button b2 = new Button("b2");
        Button b3 = new Button("b3");


        b1.setLayoutX(0);
        b1.setLayoutY(10);

        b2.setLayoutX(10);
        b2.setLayoutY(10);

        b3.setLayoutX(50);
        b3.setLayoutY(20);


        Group group = new Group();
        group.getChildren().addAll(b1, b2, b3);

        // 影响组件
        group.setOpacity(0.5);

        //包含是否有子组件
        System.out.println(group.contains(10, 20));

        //返回子组件
        Object[] objects = group.getChildren().toArray();
        System.out.println(objects.length);

        //监听
        group.getChildren().addListener(new ListChangeListener<Node>() {
            @Override
            public void onChanged(Change<? extends Node> c) {
                System.out.println("当前数量=" + c.getList().size());
            }
        });


        b3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                i = i + 10;
                Button b4 = new Button("b4");
                b4.setLayoutX(100);
                b4.setLayoutY(i);
                group.getChildren().add(b4);
            }
        });

        Scene scene = new Scene(group);
        primaryStage.setTitle("group");
        primaryStage.setWidth(500);
        primaryStage.setHeight(500);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
