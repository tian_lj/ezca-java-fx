package org.ezca.demo1;

import javafx.application.Application;
import javafx.application.ConditionalFeature;
import javafx.application.Platform;
import javafx.stage.Stage;

/**
 * @Author tianlj
 * @Date 2022 12 06 20 36
 * <p>
 * 平台相关方法
 **/
public class PlatFromMain extends Application {


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println("start=" + Thread.currentThread().getName());

        //关闭界面，不关闭程序
        Platform.setImplicitExit(true);
        //退出程序
        Platform.exit();

        //查看是否支持什么,3D什么的
        Platform.isSupported(ConditionalFeature.SCENE3D);

        //开启一个线程
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                System.out.println("开启的新线程=" + Thread.currentThread().getName());
                int i = 0;
                while (i <= 10) {

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    i++;
                    System.out.println(i);
                }


            }
        });

    }
}
