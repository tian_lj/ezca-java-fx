package org.ezca.demo1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.*;
import javafx.stage.Stage;

import java.nio.file.StandardOpenOption;

/**
 * @Author tianlj
 * @Date 2022 12 08 22 21
 **/
public class FromMain1 extends Application {
    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {


        Group group = new Group();
        Scene scene = new Scene(group);

        primaryStage.setTitle("表单类型");
        primaryStage.setWidth(800);
        primaryStage.setHeight(800);


        primaryStage.setScene(scene);

        TextField textField = new TextField();
        textField.setText("输入文本");
        textField.setPrefWidth(100);
        textField.setLayoutY(100);
        Tooltip tooltip =  new Tooltip("你好");
        textField.setTooltip(tooltip);


        group.getChildren().add(textField);


        primaryStage.show();


    }

}
