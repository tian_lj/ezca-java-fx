package org.ezca.demo1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;


/**
 * @Author tianlj
 * @Date 2022 12 07 18 55
 **/
public class ButtonMain2 extends Application {
    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {

        Button b1 = new Button("单击");
        b1.setLayoutX(10);
        b1.setLayoutY(10);


        Button b2 = new Button("双击");
        b2.setLayoutX(50);
        b2.setLayoutY(50);

        Group group = new Group();

        group.getChildren().addAll(b1, b2);
        Scene scene = new Scene(group);

        b1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("鼠标单击");
            }
        });

        b2.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                //鼠标按键==PRIMARY 左键
                //鼠标按键==SECONDARY 右键
                //鼠标按键==MIDDLE 滚轮
//                System.out.println("鼠标按键=="+event.getButton().name());
                if (event.getClickCount()==2&&event.getButton().name().equals("PRIMARY")) {
                    System.out.println("鼠标左键双击");
                }
            }
        });


        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
