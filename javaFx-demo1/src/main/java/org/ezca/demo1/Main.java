package org.ezca.demo1;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * 体现生命周期
 */
public class Main extends Application {


    public static void main(String[] args) {
        System.out.println("main===="+Thread.currentThread().getName());
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        System.out.println("start===="+Thread.currentThread().getName());
        stage.setTitle("Hello World");
        stage.show();
    }

    @Override
    public void init() throws Exception {
        System.out.println("init===="+Thread.currentThread().getName());
        super.init();
    }

    @Override
    public void stop() throws Exception {
        System.out.println("stop===="+Thread.currentThread().getName());
        super.stop();
    }
}
