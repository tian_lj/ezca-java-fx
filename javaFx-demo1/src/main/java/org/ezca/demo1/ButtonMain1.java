package org.ezca.demo1;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * @Author tianlj
 * @Date 2022 12 06 21 41
 **/
public class ButtonMain1 extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception {
        Button b1 = new Button("我是button");
        b1.setLayoutX(100);
        b1.setLayoutY(100);

        b1.setFont(Font.font("sans-serif", 40));

        BackgroundFill bgf = new BackgroundFill(Paint.valueOf("#BFBCBF"), new CornerRadii(20), new Insets(10));

        Background bg = new Background(bgf);
        b1.setBackground(bg);

        //设置外边框
        BorderStroke borderStroke = new BorderStroke(Paint.valueOf("#D72727"), BorderStrokeStyle.SOLID, new CornerRadii(20), new BorderWidths(10));
        Border border = new Border(borderStroke);
        b1.setBorder(border);


        b1.setStyle("-fx-background-color: red");

        Group group = new Group();
        group.getChildren().addAll(b1);

        Scene scene = new Scene(group);
        primaryStage.setTitle("Button");
        primaryStage.setWidth(500);
        primaryStage.setHeight(500);
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
