package org.ezca.demo1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.*;
import javafx.stage.Stage;

/**
 * @Author tianlj
 * @Date 2022 12 08 22 09
 * <p>
 * 设置快捷键
 **/
public class ButtonMian3 extends Application {

    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) throws Exception {

        Button b1 = new Button("单击");
        b1.setLayoutX(10);
        b1.setLayoutY(10);


        Button b2 = new Button("双击");
        b2.setLayoutX(50);
        b2.setLayoutY(50);

        Group group = new Group();

        group.getChildren().addAll(b1, b2);
        Scene scene = new Scene(group);


        b1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("鼠标单击");
            }
        });

        //第一种
        KeyCombination k1 = new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN, KeyCombination.CONTROL_DOWN);

        Mnemonic mnemonic1 = new Mnemonic(b1, k1);
        scene.addMnemonic(mnemonic1);


        //第二种
        KeyCombination k2 = new KeyCharacterCombination("O", KeyCombination.ALT_DOWN);

        Mnemonic mnemonic2 = new Mnemonic(b1, k2);
        scene.addMnemonic(mnemonic2);


        //第三种 ctrl+c
        KeyCombination k3 = new KeyCodeCombination(KeyCode.C, KeyCombination.SHORTCUT_DOWN);
        scene.getAccelerators().put(k3, new Runnable() {
            @Override
            public void run() {
                System.out.println("执行run()");
            }
        });

        Mnemonic mnemonic3 = new Mnemonic(b1, k3);
        scene.addMnemonic(mnemonic3);


        primaryStage.setScene(scene);
        primaryStage.show();


    }
}
