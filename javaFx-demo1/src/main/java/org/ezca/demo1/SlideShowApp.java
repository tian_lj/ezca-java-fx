package org.ezca.demo1;

import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;

/**
 * @Author tianlj
 * @Date 2023 02 22 09 48
 **/
public class SlideShowApp extends Application {

    private Image[] imageURLs = {
            new Image("https://www.bekwam.net/images/bekwam_rc_charging.png"),
            new Image("https://www.bekwam.net/images/bekwam_rc_discharging.png"),
            new Image("https://www.bekwam.net/images/bekwam_rl_scope.png")
    };

    @Override
    public void start(Stage primaryStage) throws Exception {

        Pagination pagination = new Pagination(imageURLs.length, 0);
        pagination.setPageFactory(
                pageIndex -> new ImageView(imageURLs[pageIndex])
        );
        Pagination  pagination2 = new Pagination(10, 0);
        pagination2.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return new Label(pageIndex + 1 + ". Lorem ipsum dolor sit amet,\n"
                        + "consectetur adipiscing elit,\n"
                        + "sed do eiusmod tempor incididunt ut\n"
                        + "labore et dolore magna aliqua.");
            }
        });

        VBox vbox = new VBox(pagination2 );

        Scene scene = new Scene(vbox);

        primaryStage.setScene( scene );
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
