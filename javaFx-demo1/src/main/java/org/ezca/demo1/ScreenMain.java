package org.ezca.demo1;

import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 * @Author tianlj
 * @Date 2022 12 06 20 52
 **/
public class ScreenMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        Screen screen = Screen.getPrimary();
        //dpi
        System.out.println("当前屏幕DPI=="+screen.getDpi());

        Rectangle2D rect1 = screen.getBounds();
        System.out.println("打印整个屏幕的高宽" + rect1.getHeight() + ":" + rect1.getWidth());
        System.out.println("打印整个屏幕的左上角" + rect1.getMinX()+"打印整个屏幕的右上角"+rect1.getMaxY());
        System.out.println("打印整个屏幕的左下角" + rect1.getMaxX()+"打印整个屏幕的右下角"+rect1.getMaxY());


        //当前屏幕显示的区域
        Rectangle2D rect2 = screen.getVisualBounds();
        System.out.println("打印当前显示的的高宽" + rect2.getHeight() + ":" + rect2.getWidth());
        System.out.println("打印当前显示的的左上角" + rect2.getMinX()+"打印当前显示的的右上角"+rect2.getMaxY());
        System.out.println("打印当前显示的的左下角" + rect2.getMaxX()+"打印当前显示的的右下角"+rect2.getMaxY());





        primaryStage.show();
    }
}
