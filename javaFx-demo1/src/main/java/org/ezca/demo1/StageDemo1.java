package org.ezca.demo1;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * @Author tianlj
 * @Date 2022 12 05 21 36
 **/
public class StageDemo1 extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("了解Stage");
        System.out.println(this.getClass().getResource("/img/favicon.ico").toString());
        //设置窗口图标
        primaryStage.getIcons().add(new Image(this.getClass().getResource("/img/favicon.ico").toString()));
        //设置最小化
//        primaryStage.setIconified(true);
        //设置最大化
//        primaryStage.setMaximized(true);

        //设置宽度和高度
        primaryStage.setWidth(300);
        primaryStage.setHeight(300);

//        primaryStage.setMaxHeight(500);
//        primaryStage.setMaxWidth(500);
//
//        primaryStage.setMinHeight(100);
//        primaryStage.setMinHeight(100);

        //可以改变窗口大小
//        primaryStage.setResizable(true);





        //动态获取窗口宽高
        primaryStage.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                System.out.println("当前高度="+newValue.doubleValue());
            }
        });

        primaryStage.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                System.out.println("当前宽度="+newValue.doubleValue());
            }
        });

        //设置全屏
        primaryStage.setFullScreen(true);
        primaryStage.setScene(new Scene(new Group()));//必须设置一个场景
        primaryStage.show();
        //关闭窗口
//        primaryStage.close();
//        Stage stage  = new Stage();
//        stage.setTitle("2222");
//        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
