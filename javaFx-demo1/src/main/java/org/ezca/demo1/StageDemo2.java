package org.ezca.demo1;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * @Author tianlj
 * @Date 2022 12 05 22 19
 * 进一步人社Stage窗口模式、模态
 **/
public class StageDemo2 extends Application {


    @Override
    public void start(Stage stage) throws Exception {

        //设置窗口背景透明度
//        stage.setOpacity(0.5);
        //设置窗口总是置顶
//        stage.setAlwaysOnTop(true);

//        stage.setWidth(500);
//        stage.setHeight(500);
//
//        //设置坐标
//        stage.setX(500);
//        stage.setY(100);
//
//        //监听窗口位置
//        stage.xProperty().addListener(new ChangeListener<Number>() {
//            @Override
//            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
//                System.out.println("窗口X坐标="+newValue.doubleValue());
//            }
//        });
//
//        stage.yProperty().addListener(new ChangeListener<Number>() {
//            @Override
//            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
//                System.out.println("窗口Y坐标="+newValue.doubleValue());
//            }
//        });
//
//        stage.show();


        //设置窗口样式
        Stage stage1 = new Stage();
        stage1.setTitle("stage1");
        stage1.initStyle(StageStyle.DECORATED);
        stage1.show();

//        Stage stage2 = new Stage();
//        stage2.setTitle("stage2");
//        stage2.initStyle(StageStyle.TRANSPARENT);
//        stage2.show();
//
//        Stage stage3 = new Stage();
//        stage3.setTitle("stage3");
//        stage3.initStyle(StageStyle.UNDECORATED);
//        stage3.show();

//        Stage stage4 = new Stage();
//        stage4.setTitle("stage4");
//        stage4.initStyle(StageStyle.UNIFIED);
//        stage4.show();

//
//        Stage stage5 = new Stage();
//        stage5.setTitle("stage5");
//        stage5.initStyle(StageStyle.UTILITY);
//        stage5.show();

        //平台工具，关闭所有的窗口
//        Platform.exit();


        /**
         * 模态化窗口
         */

//        Stage stage1 = new Stage();
//        stage1.setTitle("stage1");
//
//
//        Stage stage2 = new Stage();
//        stage2.setTitle("stage2");
//        //s2关联s1,即s1是s2的拥有者
//        stage2.initOwner(stage1);
//        stage2.initModality(Modality.WINDOW_MODAL);
//
//        Stage stage3 = new Stage();
//        stage3.setTitle("stage3");
//
////        stage3.initModality(Modality.APPLICATION_MODAL);
//
//        stage1.show();
//        stage2.show();
//        stage3.show();

    }

    public static void main(String[] args) {


        launch(args);
    }
}
