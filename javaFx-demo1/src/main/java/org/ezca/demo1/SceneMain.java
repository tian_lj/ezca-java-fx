package org.ezca.demo1;

import javafx.application.Application;
import javafx.application.HostServices;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * @Author tianlj
 * @Date 2022 12 06 21 04
 * <p>
 * scene 相关的知识
 **/
public class SceneMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        //打开网页地址
        HostServices hostServices = getHostServices();
        hostServices.showDocument("www.baidu.com");

        Button button = new Button("我是按钮");

        Button button2 = new Button("我是按钮3");
//        Scene scene = new Scene(button);
        Group group = new Group();
        group.getChildren().add(button);
//        group.getChildren().add(button2);
        Scene scene = new Scene(group);


        primaryStage.setScene(scene);
        primaryStage.setHeight(500);
        primaryStage.setWidth(500);

        primaryStage.show();
    }
}
